import json
import boto3
import os
from xml.dom import minidom
import urllib3

dynamodbClient = boto3.client("dynamodb",region_name='us-east-2')
s3Client = boto3.client("s3",region_name='us-east-2')

TABLA_COMPRAS = os.environ['TABLA_COMPRAS']

def hello(event, context):
    bucket = event['Records'][0]['s3']['bucket']['name']
    key = event['Records'][0]['s3']['object']['key']
    print(bucket)
    print(key)
    print("----leyendo objeto---")
    fileobj = s3Client.get_object(
        Bucket= bucket,
        Key=key,
    )

    # open the file object and read it into the variable filedata.
    filedata = fileobj['Body'].read()

    # file data will be a binary stream.  We have to decode it
    contents = filedata.decode('utf-8')

    # Once decoded, you can treat the file as plain text if appropriate
    print(contents)

    # parse xml
    xmldoc = minidom.parseString(contents)
    print("-----------------leyendo----------")
    version = xmldoc.getElementsByTagName('cbc:UBLVersionID')[0].firstChild.data
    id = xmldoc.getElementsByTagName('cbc:ID')[0].firstChild.data
    fechaemision = xmldoc.getElementsByTagName('cbc:IssueDate')[0].firstChild.data
    fechavencimiento = xmldoc.getElementsByTagName('cbc:DueDate')[0].firstChild.data

    print(version)
    print(id)
    print(fechaemision)
    print(fechavencimiento)

    supplier = xmldoc.getElementsByTagName('cac:AccountingSupplierParty')[0]
    party = supplier.getElementsByTagName('cac:Party')[0]
    partyidentification = party.getElementsByTagName('cac:PartyIdentification')[0]
    identificacion = partyidentification.getElementsByTagName('cbc:ID')[0].firstChild.data
    legalparty = party.getElementsByTagName('cac:PartyLegalEntity')[0]
    legalpartyname = legalparty.getElementsByTagName('cbc:RegistrationName')[0].firstChild.data
    print(identificacion)
    print(legalpartyname)

    monetaryTotal = xmldoc.getElementsByTagName('cac:LegalMonetaryTotal')[0]
    subtotal = monetaryTotal.getElementsByTagName('cbc:LineExtensionAmount')[0].firstChild.data
    totalconimpuesto = monetaryTotal.getElementsByTagName('cbc:TaxInclusiveAmount')[0].firstChild.data
    print(subtotal)
    print(totalconimpuesto)


    print(id.split('-')[0])
    fecha= fechaemision.split('-')
    fechaemision = fecha[2]+'/'+fecha[1]+'/'+fecha[0]
    print(fechaemision)
    data = {
        "numRuc": identificacion,
        "codComp": "01",
        "numeroSerie": id.split('-')[0],
        "numero": id.split('-')[1],
        "fechaEmision": fechaemision,
        "monto": totalconimpuesto
    }

    response = validarComprobante2(data)

    print (response)
    #if response['success']:
    estadoCp = response['data']['estadoCp']
    estadoRuc = response['data']['estadoRuc']
    condDomiRuc = response['data']['condDomiRuc']

    print("----agregado a dynamo---")

    response = dynamodbClient.put_item(
        TableName=TABLA_COMPRAS,
        Item={
            'serieCorrelativo': {'S': id},
            'fechaemision': {'S': fechaemision},
            'fechavencimiento': {'S': fechavencimiento},
            'identificacion': {'S': identificacion},
            'legalpartyname': {'S': legalpartyname},
            'subtotal': {'S': subtotal},
            'totalconimpuesto': {'S': totalconimpuesto},
            'estadoCp': {'S': estadoCp},
            'estadoRuc': {'S': estadoRuc},
            'condDomiRuc': {'S': condDomiRuc},

        }
    )

    return response

def validarComprobante2(data):
    print("validar comprobante 2 ")
    ruc= data['numRuc']

    url= 'https://api.sunat.gob.pe/v1/contribuyente/contribuyentes/'+ruc+'/validarcomprobante'
    token = 'eyJraWQiOiJhcGkuc3VuYXQuZ29iLnBlLmtpZDEwMSIsInR5cCI6IkpXVCIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiIzZGQ3ZjBlYi02Zjc1LTQ2NGQtOWUyOS04MDYyNDkwNjlmOTgiLCJhdWQiOiJbe1wiYXBpXCI6XCJodHRwczpcL1wvYXBpLnN1bmF0LmdvYi5wZVwiLFwicmVjdXJzb1wiOlt7XCJpZFwiOlwiXC92MVwvY29udHJpYnV5ZW50ZVwvY29udHJpYnV5ZW50ZXNcIixcImluZGljYWRvclwiOlwiMFwiLFwiZ3RcIjpcIjAxMDAwMFwifSx7XCJpZFwiOlwiXC92MVwvY29udHJvbGFkdWFuZXJvXC9kZWNsYXJhY2lvbmVzdGFpXCIsXCJpbmRpY2Fkb3JcIjpcIjBcIixcImd0XCI6XCIwMTAwMDBcIn0se1wiaWRcIjpcIlwvdjFcL2NvbnRyb2xhZHVhbmVyb1wvc29saWNpdHVkZXNkZWNsYXJhY2lvbnRhaVwiLFwiaW5kaWNhZG9yXCI6XCIwXCIsXCJndFwiOlwiMDEwMDAwXCJ9XX0se1wiYXBpXCI6XCJodHRwczpcL1wvYXBpLnN1bmF0LmdvYi5wZVwiLFwicmVjdXJzb1wiOlt7XCJpZFwiOlwiXC92MVwvY29udHJpYnV5ZW50ZVwvY29udHJpYnV5ZW50ZXNcIixcImluZGljYWRvclwiOlwiMFwiLFwiZ3RcIjpcIjAxMDAwMFwifSx7XCJpZFwiOlwiXC92MVwvY29udHJvbGFkdWFuZXJvXC9kZWNsYXJhY2lvbmVzdGFpXCIsXCJpbmRpY2Fkb3JcIjpcIjBcIixcImd0XCI6XCIwMTAwMDBcIn0se1wiaWRcIjpcIlwvdjFcL2NvbnRyb2xhZHVhbmVyb1wvc29saWNpdHVkZXNkZWNsYXJhY2lvbnRhaVwiLFwiaW5kaWNhZG9yXCI6XCIwXCIsXCJndFwiOlwiMDEwMDAwXCJ9XX0se1wiYXBpXCI6XCJodHRwczpcL1wvYXBpLnN1bmF0LmdvYi5wZVwiLFwicmVjdXJzb1wiOlt7XCJpZFwiOlwiXC92MVwvY29udHJpYnV5ZW50ZVwvY29udHJpYnV5ZW50ZXNcIixcImluZGljYWRvclwiOlwiMFwiLFwiZ3RcIjpcIjAxMDAwMFwifSx7XCJpZFwiOlwiXC92MVwvY29udHJvbGFkdWFuZXJvXC9kZWNsYXJhY2lvbmVzdGFpXCIsXCJpbmRpY2Fkb3JcIjpcIjBcIixcImd0XCI6XCIwMTAwMDBcIn0se1wiaWRcIjpcIlwvdjFcL2NvbnRyb2xhZHVhbmVyb1wvc29saWNpdHVkZXNkZWNsYXJhY2lvbnRhaVwiLFwiaW5kaWNhZG9yXCI6XCIwXCIsXCJndFwiOlwiMDEwMDAwXCJ9XX0se1wiYXBpXCI6XCJodHRwczpcL1wvd3Muc3VuYXQuZ29iLnBlXCIsXCJyZWN1cnNvXCI6bnVsbH0se1wiYXBpXCI6XCJodHRwczpcL1wvd3Muc3VuYXQuZ29iLnBlXCIsXCJyZWN1cnNvXCI6bnVsbH1dIiwibmJmIjoxNTkxMzMxOTM0LCJjbGllbnRJZCI6IjNkZDdmMGViLTZmNzUtNDY0ZC05ZTI5LTgwNjI0OTA2OWY5OCIsImlzcyI6Imh0dHBzOlwvXC9hcGktc2VndXJpZGFkLnN1bmF0LmdvYi5wZVwvdjFcL2NsaWVudGVzZXh0cmFuZXRcLzNkZDdmMGViLTZmNzUtNDY0ZC05ZTI5LTgwNjI0OTA2OWY5OFwvb2F1dGgyXC90b2tlblwvIiwiZXhwIjoxNTkxMzM1NTM0LCJncmFudFR5cGUiOiJjbGllbnRfY3JlZGVudGlhbHMiLCJpYXQiOjE1OTEzMzE5MzR9.26s8gKmPLiGcsggaXmocO7C0rdBw5yDDQgDtbow_SMLcauOhwndkY8icyErOhMYYQQblNkFacAtXSilIMI3O_nvxBN_HYuz_ZXftyga5oLxvKLCP3HEB8uq_Hoh-uVUmndPlpBbuf5wXqFO9lZS1ksFZu-KbkLfV7az2rcxF50kB0bQR3dvpOME4WkvgCY6fY6xHgtG_lAI0C7HBRc32po6AgU2PEzQAJLqFutlJLFyZDS4hQxWykWXVuSOTms4E0q6qiQIh9eClUF-a2oknaPqFq4Ew6O0yivDP4Y5yxaOeu3qMkjaSo56RxDXr00o6lJeSwBnBPrACYOYENS7WnA'

    headers = {
        "Content-Type": "application/json",
        "Authorization": 'Bearer '+token
    }
    http = urllib3.PoolManager()
    r = http.request('POST',url, headers=headers, body=json.dumps(data))
    response = json.loads(r.data.decode('utf-8'))
    print (response)
    return response

def validarComprobante(event, context):
    print("validar comprobante")
    ruc='20132373524'
    url= 'https://api.sunat.gob.pe/v1/contribuyente/contribuyentes/'+ruc+'/validarcomprobante'
    token = 'eyJraWQiOiJhcGkuc3VuYXQuZ29iLnBlLmtpZDEwMSIsInR5cCI6IkpXVCIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiIzZGQ3ZjBlYi02Zjc1LTQ2NGQtOWUyOS04MDYyNDkwNjlmOTgiLCJhdWQiOiJbe1wiYXBpXCI6XCJodHRwczpcL1wvYXBpLnN1bmF0LmdvYi5wZVwiLFwicmVjdXJzb1wiOlt7XCJpZFwiOlwiXC92MVwvY29udHJpYnV5ZW50ZVwvY29udHJpYnV5ZW50ZXNcIixcImluZGljYWRvclwiOlwiMFwiLFwiZ3RcIjpcIjAxMDAwMFwifSx7XCJpZFwiOlwiXC92MVwvY29udHJvbGFkdWFuZXJvXC9kZWNsYXJhY2lvbmVzdGFpXCIsXCJpbmRpY2Fkb3JcIjpcIjBcIixcImd0XCI6XCIwMTAwMDBcIn0se1wiaWRcIjpcIlwvdjFcL2NvbnRyb2xhZHVhbmVyb1wvc29saWNpdHVkZXNkZWNsYXJhY2lvbnRhaVwiLFwiaW5kaWNhZG9yXCI6XCIwXCIsXCJndFwiOlwiMDEwMDAwXCJ9XX0se1wiYXBpXCI6XCJodHRwczpcL1wvYXBpLnN1bmF0LmdvYi5wZVwiLFwicmVjdXJzb1wiOlt7XCJpZFwiOlwiXC92MVwvY29udHJpYnV5ZW50ZVwvY29udHJpYnV5ZW50ZXNcIixcImluZGljYWRvclwiOlwiMFwiLFwiZ3RcIjpcIjAxMDAwMFwifSx7XCJpZFwiOlwiXC92MVwvY29udHJvbGFkdWFuZXJvXC9kZWNsYXJhY2lvbmVzdGFpXCIsXCJpbmRpY2Fkb3JcIjpcIjBcIixcImd0XCI6XCIwMTAwMDBcIn0se1wiaWRcIjpcIlwvdjFcL2NvbnRyb2xhZHVhbmVyb1wvc29saWNpdHVkZXNkZWNsYXJhY2lvbnRhaVwiLFwiaW5kaWNhZG9yXCI6XCIwXCIsXCJndFwiOlwiMDEwMDAwXCJ9XX0se1wiYXBpXCI6XCJodHRwczpcL1wvYXBpLnN1bmF0LmdvYi5wZVwiLFwicmVjdXJzb1wiOlt7XCJpZFwiOlwiXC92MVwvY29udHJpYnV5ZW50ZVwvY29udHJpYnV5ZW50ZXNcIixcImluZGljYWRvclwiOlwiMFwiLFwiZ3RcIjpcIjAxMDAwMFwifSx7XCJpZFwiOlwiXC92MVwvY29udHJvbGFkdWFuZXJvXC9kZWNsYXJhY2lvbmVzdGFpXCIsXCJpbmRpY2Fkb3JcIjpcIjBcIixcImd0XCI6XCIwMTAwMDBcIn0se1wiaWRcIjpcIlwvdjFcL2NvbnRyb2xhZHVhbmVyb1wvc29saWNpdHVkZXNkZWNsYXJhY2lvbnRhaVwiLFwiaW5kaWNhZG9yXCI6XCIwXCIsXCJndFwiOlwiMDEwMDAwXCJ9XX0se1wiYXBpXCI6XCJodHRwczpcL1wvd3Muc3VuYXQuZ29iLnBlXCIsXCJyZWN1cnNvXCI6bnVsbH0se1wiYXBpXCI6XCJodHRwczpcL1wvd3Muc3VuYXQuZ29iLnBlXCIsXCJyZWN1cnNvXCI6bnVsbH1dIiwibmJmIjoxNTkxMzMxOTM0LCJjbGllbnRJZCI6IjNkZDdmMGViLTZmNzUtNDY0ZC05ZTI5LTgwNjI0OTA2OWY5OCIsImlzcyI6Imh0dHBzOlwvXC9hcGktc2VndXJpZGFkLnN1bmF0LmdvYi5wZVwvdjFcL2NsaWVudGVzZXh0cmFuZXRcLzNkZDdmMGViLTZmNzUtNDY0ZC05ZTI5LTgwNjI0OTA2OWY5OFwvb2F1dGgyXC90b2tlblwvIiwiZXhwIjoxNTkxMzM1NTM0LCJncmFudFR5cGUiOiJjbGllbnRfY3JlZGVudGlhbHMiLCJpYXQiOjE1OTEzMzE5MzR9.26s8gKmPLiGcsggaXmocO7C0rdBw5yDDQgDtbow_SMLcauOhwndkY8icyErOhMYYQQblNkFacAtXSilIMI3O_nvxBN_HYuz_ZXftyga5oLxvKLCP3HEB8uq_Hoh-uVUmndPlpBbuf5wXqFO9lZS1ksFZu-KbkLfV7az2rcxF50kB0bQR3dvpOME4WkvgCY6fY6xHgtG_lAI0C7HBRc32po6AgU2PEzQAJLqFutlJLFyZDS4hQxWykWXVuSOTms4E0q6qiQIh9eClUF-a2oknaPqFq4Ew6O0yivDP4Y5yxaOeu3qMkjaSo56RxDXr00o6lJeSwBnBPrACYOYENS7WnA'
    data = {
        "numRuc":"20538455840",
        "codComp":"01",
        "numeroSerie":"F001",
        "numero":"3642",
        "fechaEmision":"01/06/2020",
        "monto":"702.84"
    }
    headers = {
        "Content-Type": "application/json",
        "Authorization": 'Bearer '+token
    }
    http = urllib3.PoolManager()
    r = http.request('POST',url, headers=headers, body=json.dumps(data))
    response = json.loads(r.data.decode('utf-8'))
    print (response)
    response = {
        "statusCode": 200,
        "body": json.dumps(response)
    }
    return response

def cargarInvoice(event, context):
    print(event)
    print("Cargarinvoice")
    response = dynamodbClient.put_item(
        TableName=TABLA_COMPRAS,
        Item={
            'serieCorrelativo': {'S': 'FUNCIONa'},

        }
    )
    print(response)

    return response